FC = gfortran

FLAGS=-O3  -ffree-form -std=f2008 -fimplicit-none -Wall -pedantic -fbounds-check -cpp

prec4:
	$(FC) $(FLAGS) -D"PREC=4" gauss.f90 main.f90  -o main4

prec8:
	$(FC) $(FLAGS) -D"PREC=8" gauss.f90 main.f90  -o main8

prec16:
	$(FC) $(FLAGS) -D"PREC=16" gauss.f90 main.f90  -o main16

clean:
	rm -f main4
	rm -f main8
	rm -f main16
	rm -f gauss.mod
