# Fortran project no 1

Solving equation using Gauss elimination

## Compilation using make and gfortran:
```
make prec4
make prec8
make prec16
```

## Running:
```
./main4 {n}
./main8 {n}
./main16 {n}
```
