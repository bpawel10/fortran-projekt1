#if !defined(PREC)
#define PREC 4
#endif

program main
    use gauss
    implicit none

    real(kind=PREC) :: u1, u2, u3
    real(kind=PREC), allocatable, dimension(:,:) :: a, result
    real(kind=PREC), allocatable, dimension(:) :: b
    real(kind=PREC), allocatable, dimension(:) :: ideal

    integer(kind=8) :: i, j, n
    character(len=10) :: arg

    call get_command_argument(1, arg)
    read(arg(1:len_trim(arg)), '(i8)') n

    u1 = real(n*n)
    u2 = -2 * u1
    u3 = u1

    allocate(a(0:n, 0:n))
    allocate(b(0:n))
    allocate(result(0:n, 0:n))

    a(:,:) = 0
    b(:) = 0
    b(n) = 1

    do i = 1, n - 1
        a(i, i) = u3
        a(i, i + 1) = u2
        if(i < n-1) a(i, i+2) = u1
    end do

    result = eliminate(a, b)

    allocate(ideal(0:n))

    ideal(0) = 0
    do i = 1, n
        ideal(i) = real(1, kind=16)/real(n, kind=16)*i
    end do

    do i = 0, n
        do j = 0, n
            print *, i, j, result(i, j)
        end do
    end do


end program main
