module gauss

contains
    function eliminate(a, b) result(u)
        real :: a(:,:),b(:), upi
        integer :: i, j, n, p
        real, allocatable :: u(:,:)

        n = size(a,1)
        u = reshape([a,b], [n,n+1])

        do j=1,n
            p = maxloc(abs(u(j:n,j)),1) + j-1

            if (p /= j) then
                u([p,j],j) = u([j,p],j)
            end if

            u(j+1:,j) = u(j+1:,j)/u(j,j)

            do i=j+1,n+1
                upi = u(p,i)

                if (p /= j) then
                    u([p,j],i) = u([j,p],i)
                end if

                u(j+1:n,i) = u(j+1:n,i) - upi*u(j+1:n,j)
            end do
        end do
    end function
end module gauss
